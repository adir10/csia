const express= require('express');
const cors = require('cors');
const mongoose= require ('mongoose')
mongoose.connect('mongodb://localhost:27017/TimeTables',{ useNewUrlParser: true });
const db= mongoose.connection;
db.on('error', console.error.bind(console, 'Connection error: '));
db.once('open', function(callback) {
//The code in this asynchronous callback block is executed after connecting to MongoDB. 
    console.log('Successfully connected to MongoDB.');
});
const app=express()
app.use(cors());
const port=5000

const Schema= mongoose.Schema;
const SlotsSchema = new Schema ({
    Day:String,
    Slot_Id:Number,
    Subject:String,
    Topic: String,
    Start_Time: Date,
    End_Time: Date,
    Duration: Number,
    Free_Time: Boolean})

const Slot = mongoose.model('Slots', SlotsSchema, "Weeks");
const getSlots =  async function(params){
    return data=[{id:0,title:'vkj.asfhk'},{id:1,title:'vkj.csjfaasfhk'}] ;// array of slots  
}   

const saveTodb = async function(err,slot){
        
    const Slot1 = new Slot({ 
        Day: "Monday",
        Slot_Id:4,
        Subject:"Basketball",
        Topic: "Practice",
        Start_Time: "2019-08-03T09:06:33.026Z",
        End_Time: "2019-08-03T09:07:33.026Z",
        Duration: 2,
        Free_Time: true });

    return Slot1.save(function (err, slot) {
    if (err) return console.error(err);
    console.log(slot.Slot_Id + " saved to timetable.");

});

};

const getFromDb= async function (){
    return Slot.find()
    .then(slots=>convertedSlots(slots))
}
const convertedSlots=(slots)=>{
    let newSlots=slots;
    newSlots=slots.map(slot=>{
        let newSlot={...slot._doc};
        newSlot.Start_Time=slot.Start_Time.toLocaleTimeString();
        newSlot.End_Time=slot.End_Time.toLocaleTimeString();
        return newSlot;
    });
    return newSlots;
}
// return 'test';
//get data from db
//     return Week.find({}, function(error, slots) {
//         if(error){
//             console.error(error);
//         }
//         console.log(`slots:${slots}`); //Display the comments returned by MongoDB, if any were found. Executes after the query is complete.
//     });
// }
// app.get('/',async (req,res)=>{
//     let data=await getDb();
//     return res.send({data:data,error:null})})
app.get('/slots', (req,res)=>{

    let params={week:1,day:'Sunday'};

    // return getSlots(params)
    return getFromDb()
    .then(result=>res.send({data:result,error:null}))
    .catch(err=>res.send({data:null,error:err.toString()}));
})
app.listen(port, () => console.log(`Example app listening on port ${port}!`))
getFromDb();